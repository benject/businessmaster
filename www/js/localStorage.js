/**
 * Created by Administrator on 2016-9-4.
 */
(function () {
  'use strict';
  angular.module('starter.services',[])
    .factory('LocalStorageService',function () {
      var service = {
        getData: function (key, defaultValue) {
          var val = localStorage.getItem(key);
          try {
            val = angular.fromJson(val);
          }
          catch (error) {
            val = null;
          }
          if (defaultValue && val === null) {
            val = defaultValue;
          }

          return val;
        },
        update: function (key, value) {
          if (value) {
            localStorage.setItem(key, angular.toJson(value));
          }
        },
        delete: function (key) {
          localStorage.removeItem(key);
        }
      };

      return service;
    });

})();
