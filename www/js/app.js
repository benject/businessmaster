// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers','starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'pages/menu/menu.html',
    controller: 'MenuCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })
    .state('app.member', {
    url: '/member',
    views: {
      'menuContent': {
        templateUrl: 'pages/members/member.html',
        controller:'MemberCtrl'
      }
    }
  })
    .state('app.editmember', {
      url: '/editmember',
      views: {
        'menuContent': {
          templateUrl: 'pages/members/editmember.html',
          controller:'EditMemberCtrl'
        }
      }
    })

    .state('app.product', {
      url: '/product',
      views: {
        'menuContent': {
          templateUrl: 'pages/product/product.html',
          controller:'ProductCtrl'
        }
      }
    })

  //editproduct
    .state('app.editproduct', {
      url: '/editproduct',
      views: {
        'menuContent': {
          templateUrl: 'pages/product/editproduct.html',
          controller: 'EditProductCtrl'
        }
      }
    })

    //登陆
    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'pages/login/login.html',
          controller:'LoginCtrl'

        }
      }
    })

    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'pages/playlists/playlists.html',
          controller: 'PlayListsCtrl'
        }
      }
    })
    //editShop
    .state('app.editshop', {
      url: '/editshop',
      params:{'title':''},
      views: {
        'menuContent': {
          templateUrl: 'pages/shop/editshop.html',
          controller: 'EditShopCtrl'
        }
      }
    })
    //SHOP
    .state('app.shop', {
      url: '/shop',

      views: {
        'menuContent': {
          templateUrl: 'pages/shop/shop.html',
          controller: 'ShopCtrl'
        }
      }
    })
    //类别
    .state('app.category',{
    url:'/category',
      views:{
        'menuContent':{
          templateUrl:'pages/category/category.html',
          controller:'CategoryCtrl'
        }
      }

    })

    //编辑类别
    .state('app.editcategory',{
      url:'/editcategory/:type',
      views:{
        'menuContent':{
          templateUrl:'pages/category/editcategory.html',
          controller:'EditCategoryCtrl'
        }
      }

    })

    //setting
  .state('app.setting', {
    url: '/setting',
    views: {
      'menuContent': {
        templateUrl: 'pages/setting/setting.html',
        controller: 'SettingCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/playlists');
});
