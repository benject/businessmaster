(function () {
    'use strict';
    angular.module('starter.controllers')
        .controller('ShopCtrl', ['$scope','$state','LocalStorageService', function ($scope,$state,LocalStorageService) {
          $scope.shopData = LocalStorageService.getData('Shop');
          LocalStorageService.update('temp',1);
          $scope.edit = function (title) {
            $state.go('app.editshop',{'title':title});

          };
          $scope.$on('$stateChangeSuccess',function () {

            $scope.shopData = LocalStorageService.getData('Shop');

          })
        }
        ]);
})();
