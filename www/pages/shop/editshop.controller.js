(function () {
    'use strict';
    angular.module('starter.controllers')
        .controller('EditShopCtrl', ['$scope','$state','$stateParams','LocalStorageService', function ($scope,$state,$stateParams,LocalStorageService) {
          $scope.title=$stateParams.title;

          $scope.data = LocalStorageService.getData('Shop');
          if($scope.data==null){
            $scope.data = {
              Name:'Bing\'s shop'
              ,AB:'Bing的小店'
              ,Boss:'陈兵'
              ,ShopPhone:'0591-38838888'
              ,CreateDate:'2016-09-01'
              ,Phone:'187xxxxxxxx'
              ,Email:'123@email.com'
            };
          }
          if($scope.title=='店铺名称'){
            $scope.value = $scope.data.Name;
          }
          else  if($scope.title=='店铺简称'){
            $scope.value =$scope.data.AB ;
          }
          else  if($scope.title=='手机号码'){
            $scope.value =$scope.data.Phone ;
          }
          else if ($scope.title === '店主姓名'){
            $scope.value = $scope.data.Boss;
          }
          else if ($scope.title === '店铺电话'){
            $scope.value = $scope.data.ShopPhone;
          }
          else if ($scope.title === '电子邮件'){
            $scope.value = $scope.data.Email;
          }

          
          $scope.save= function () {
            if ($scope.title === '店铺名称'){
              $scope.data.Name = $scope.value;
            }
            else if ($scope.title === '店铺简称'){
              $scope.data.AB = $scope.value;
            }
            else if ($scope.title === '手机号码'){
              $scope.data.Phone = $scope.value;
            }
            else if ($scope.title === '店主姓名'){
              $scope.data.Boss = $scope.value;
            }
            else if ($scope.title === '店铺电话'){
              $scope.data.ShopPhone = $scope.value;
            }
            else if ($scope.title === '电子邮件'){
              $scope.data.Email = $scope.value;
            }
          LocalStorageService.update('Shop',$scope.data);
          $state.go('app.shop');
          }
        }]);
})();
