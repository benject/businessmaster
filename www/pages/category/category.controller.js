/**
 * Created by Administrator on 2016-9-1.
 */

(function () {
'use strict';
  angular.module('starter.controllers')
    .controller('CategoryCtrl',['$scope', 'CategoryService','$ionicHistory',function ($scope, CategoryService, $ionicHistory) {


      $scope.activeSection = null;
      $scope.$watch('activeSection', function(newValue, oldValue){
        CategoryService.updateCategory($scope.activeSection);
      });

      $scope.categories = [
        {
          ID:1
          ,Name:'电脑整机'
          ,Children:[
          {
            ID:5
            ,Name:'笔记本'
            ,Children:[]
          }
          ,{
            ID:6
            ,Name:'台式机'
            ,Children:[]
          }
          ,{
            ID:7
            ,Name:'平板电脑'
            ,Children:[]
          }
        ]
        }
        ,{
          ID:2
          ,Name:'电脑配件'
          ,Children:[
            {
              ID:8
              ,Name:'CPU'
              ,Children:[]
            }
            ,{
              ID:9
              ,Name:'内存'
              ,Children:[]
            }]
        }
        ,{
          ID:3
          ,Name:'外设产品'
          ,Children:[
            {
              ID:10
              ,Name:'鼠标'
              ,Children:[]
            }
            ,{
              ID:11
              ,Name:'键盘'
              ,Children:[]
            }
            ,{
              ID:12
              ,Name:'U 盘'
              ,Children:[]
            }
          ]
        }
        ,{
          ID:4
          ,Name:'网络产品'
          ,Children:[
            {
              ID:13
              ,Name:'路由器'
              ,Children:[]
            }
            ,{
              ID:14
              ,Name:'交换机'
              ,Children:[]
            }
            ,{
              ID:15
              ,Name:'网卡'
              ,Children:[]
            }
            ,{
              ID:16,Name:'网络配件'
              ,Children:[]
            }
          ]
        }
      ];
      $scope.activeID = $scope.categories[0].ID;
      $scope.sections = $scope.categories[0].Children;
      $scope.selectCategory= function (id) {
        console.log("select categor id=", id);
        if($scope.activeID!=id){
          $scope.activeID=id;
          angular.forEach($scope.categories,function (data,index) {
            if(data.ID == id){
              $scope.sections = data.Children;
            }
          })
        }
      };
      $scope.select = function (selectItem) {
        $scope.activeSection = selectItem;
        $ionicHistory.goBack();
      }

      $scope.showActionSheet = function () {
        $ionicActionSheet.show({

          buttons:[{text:'new big cate'},{text:'new small cate'},{text:'edit big cate'}]
          ,cancelText:'quxio'
          ,buttonClicked:function (index) {
            switch(index){
              case 0:
                    $stage.go('login');
                    break;
              case 1:
                $stage.go('app.editcategory');
                    break;
              case 2:
                    break;
            }

          }
                  })
      }

    }]);

angular.module('starter.services')
    .factory('CategoryService',function ($rootScope) {
      var service={};
      service.activeCategory = {};
      service.updateCategory = function (value) {
        this.activeCategory = angular.copy(value);
        $rootScope.$broadcast('CategoryUpdate');

      };
     return service;
    });
})();
