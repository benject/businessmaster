/**
 * Created by Administrator on 2016-9-2.
 */
(function () {
    'use strict';
    angular.module('starter.controllers')
      .controller('EditCategoryCtrl',['$scope','$state','$stateParams',function ($scope,$state,$stateParams) {

        $scope.type = $stateParams.type;
        $scope.categories =[{Name:''}];
        $scope.addCategories = function () {
          $scope.categories.push({Name:''});
          $state.go('app.category');
        }
        $scope.sections =[{Name:''}];
        $scope.addSection = function () {
          $scope.sections.push({Name:''});
          $state.go('app.category');

        }

      }])
  }

)();
