/**
 * Created by Administrator on 2016-11-4.
 */
(function () {
  'use strict';
  angular.module('starter.controllers')
    .controller('EditProductCtrl', ['$scope','LocalStorageService','$timeout',  function ($scope,LocalStorageService,$timeout) {


      $scope.product=LocalStorageService.getData('pdtList', []);
      $scope.clearLocalStorage=function () {

        localStorage.removeItem('pdtList')
      }
      var pageIndex=1;
      var isLoading=false;
      $scope.doRefresh=function () {
        pageIndex=1;
        if(isLoading){
          return;
        }
        isLoading=true;
        $timeout(function () {

          $scope.product=LocalStorageService.getData('pdtList', []);
          isLoading=false;
          $scope.$broadcast('scroll.refreshComplete');

        },500);

      }


    }]);
})();
