(function () {
  'use strict';
  angular.module('starter.controllers')
    .controller('ProductCtrl', ['$scope', 'CategoryService', '$cordovaBarcodeScanner',
      '$ionicActionSheet', '$cordovaCamera', '$cordovaImagePicker', '$ionicPopup',
      'LocalStorageService',
      function ($scope, CategoryService, $cordovaBarcodeScanner,
                $ionicActionSheet, $cordovaCamera, $cordovaImagePicker, $ionicPopup,
                LocalStorageService
      ) {
        

        //商品信息数据结构
        var pdtInfo = {
          imgs: [],
          skuBarcode: '',
          name: '',
          salePrice: '',
          purchasePrice: '',
          storeCnt: '',
          spec: '',
          remark: '',
          supplier: {name: '', tel: ''}
        };
        $scope.pdtInfo = pdtInfo;


        $scope.$on('CategoryUpdate', function () {
          console.log('on called');
          console.log(CategoryService.activeCategory);
          pdtInfo.category = CategoryService.activeCategory;
        });


        $scope.scanBarcode = function () {
          $cordovaBarcodeScanner.scan().then(function (barcodeData) {
            console.log(barcodeData.text);
            pdtInfo.skuBarcode = barcodeData.text;
          }, function (error) {
            console.log("barcode scan cancel or error.")
          });
        };

        $scope.showActionSheet = function () {

          if (pdtInfo.imgs.length >=3){
            alert("抱歉,目前系统最多只支持上传3张照片!");
          } else {
            $ionicActionSheet.show({
              buttons: [{text: '拍照'}, {text: '从相册中选取'}]
              , cancelText: '<b>取消</b>'
              , buttonClicked: function (index) {
                switch (index) {
                  case 0:
                    camera();
                    break;
                  case 1:
                    pickImage();
                    break;
                  default:
                }
              }
            });
          }
        };

        function camera() {
          var options = {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceTYpe: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 100,
            targetHeight: 100,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
          };

          $cordovaCamera.getPicture(options).then(function (imageData) {
            // var image = document.getElementById('myImage');
            // image.src = "data:image/jpeg:base64," + imageData;
            console.log(">>>>>>>>>>>>>>>>" + imageData.substring(0, 50));
            pdtInfo.imgs.push({imgData:"data:image/jpeg;base64," + imageData});

          }, function (err) {
            console.log("get pic error", err);
          });
        }

        function pickImage() {
          var options = {
            maximumImagesCount: 3,
            width: 0,
            height: 0,
            quality: 80
          };
          $cordovaImagePicker.getPictures(options).then(function (results) {
            pdtInfo.imgs = [];
            for (var i = 0; (i < results.length && i < 3); i++) {
              console.log('ImageUrl:' + results[i]);
              pdtInfo.imgs.push({imgLocalUrl: results[i]});
            }
          }, function (error) {

          });
        }

        $scope.selectSupplier = function(){
          if (!pdtInfo.supplier) {
            pdtInfo.supplier = {};
          }
          var supplierInfo = angular.copy(pdtInfo.supplier);
          $scope.supplierInfo = supplierInfo;
          $ionicPopup.show({
            title: '新增供应商',
            template: '<div class="list pop-form"> \
            <label class="item item-input"> \
            <span class="input-label">供应商名称<em>*</em></span> \
            <input type="text" ng-model="supplierInfo.name"> \
            </label> \
            <label class="item item-input"> \
            <span class="input-label">供应商电话<em>*</em></span> \
            <input type="text" ng-model="supplierInfo.tel"> \
            </label> \
            </div>',
            scope: $scope,
            buttons:[
              {
                text:'取消',
                type: 'button-default',
                onTap: function(){

                  // e.preventDefault();
                }
              },{
                text:'确定',
                type: 'button-positive',
                onTap: function(e){
                  var isValid = true;
                  if (!supplierInfo.name) {
                    alert('请输入供应商名称!');
                    isValid = false;
                  }

                  if (!supplierInfo.tel) {
                    alert('请输入供应商电话!');
                    isValid = false;
                  }
                  if (!isValid) {
                    e.preventDefault();
                  } else {
                    pdtInfo.supplier = {
                      name: supplierInfo.name,
                      tel: supplierInfo.tel
                    };
                  }
                }
              }
            ]
          })
        };

        $scope.doSave = function (noAlert) {
          var isValid = true;
          if (!pdtInfo.name) {
            alert("请输入名称");
            isValid = false;
          }
          if (!pdtInfo.category) {
            alert("请选择分类");
            isValid = false;
          }
          if (!pdtInfo.salePrice) {
            alert("请输入售价");
            isValid = false;
          }
          if (isValid) {
            var pdtList = LocalStorageService.getData('pdtList', []);
            pdtList.push(pdtInfo);
            LocalStorageService.update('pdtList', pdtList);
            if (!noAlert) {
              alert('添加成功!')
            }
            return true;
          } else {
            return false;
          }
        };

        $scope.doSaveAndNew = function () {
          if ($scope.doSave(true)) {
            pdtInfo = {imgs:[]};
            $scope.pdtInfo = pdtInfo;
          }
        };

      }]);
})();
