(function () {
  'use strict';
  angular.module('starter.controllers')
    .controller('MemberCtrl', ['$scope', 'CategoryService', '$cordovaBarcodeScanner',
      '$ionicActionSheet', '$cordovaCamera', '$cordovaImagePicker', '$ionicPopup',
      'LocalStorageService',
      function ($scope, CategoryService, $cordovaBarcodeScanner,
                $ionicActionSheet, $cordovaCamera, $cordovaImagePicker, $ionicPopup,
                LocalStorageService
      ) {



        //会员信息数据结构
        var memberInfo = {
          imgs: [],
          number:'',
          name: '',
          tel: '',
          genre: '',
          birthday: '',
          address: '',
          email:'',
        };
        $scope.memberInfo = memberInfo;


        $scope.$on('CategoryUpdate', function () {
          console.log('on called');
          console.log(CategoryService.activeCategory);
          memberInfo.category = CategoryService.activeCategory;
        });




        $scope.showActionSheet = function () {

          if (memberInfo.imgs.length >=3){
            alert("抱歉,目前系统最多只支持上传3张照片!");
          } else {
            $ionicActionSheet.show({
              buttons: [{text: '拍照'}, {text: '从相册中选取'}]
              , cancelText: '<b>取消</b>'
              , buttonClicked: function (index) {
                switch (index) {
                  case 0:
                    camera();
                    break;
                  case 1:
                    pickImage();
                    break;
                  default:
                }
              }
            });
          }
        };

        function camera() {
          var options = {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceTYpe: Camera.PictureSourceType.CAMERA,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 100,
            targetHeight: 100,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
          };

          $cordovaCamera.getPicture(options).then(function (imageData) {
            // var image = document.getElementById('myImage');
            // image.src = "data:image/jpeg:base64," + imageData;
            console.log(">>>>>>>>>>>>>>>>" + imageData.substring(0, 50));
            memberInfo.imgs.push({imgData:"data:image/jpeg;base64," + imageData});

          }, function (err) {
            console.log("get pic error", err);
          });
        }

        function pickImage() {
          var options = {
            maximumImagesCount: 3,
            width: 0,
            height: 0,
            quality: 80
          };
          $cordovaImagePicker.getPictures(options).then(function (results) {
            memberInfo.imgs = [];
            for (var i = 0; (i < results.length && i < 3); i++) {
              console.log('ImageUrl:' + results[i]);
              memberInfo.imgs.push({imgLocalUrl: results[i]});
            }
          }, function (error) {

          });
        }


        $scope.doSave = function (noAlert) {
          var isValid = true;
          if (!memberInfo.number) {
            alert("请输入卡号");
            isValid = false;
          }
          if (!memberInfo.name) {
            alert("请输入姓名");
            isValid = false;
          }
          if (!memberInfo.tel) {
            alert("请输入电话");
            isValid = false;
          }
          if (isValid) {
            var memberList = LocalStorageService.getData('memberList', []);
            memberList.push(memberInfo);
            LocalStorageService.update('memberList', memberList);
            if (!noAlert) {
              alert('添加成功!')
            }
            return true;
          } else {
            return false;
          }
        };

        $scope.doSaveAndNew = function () {
          if ($scope.doSave(true)) {
            memberInfo = {imgs:[]};
            $scope.pdtInfo = pdtInfo;
          }
        };

      }]);
})();
