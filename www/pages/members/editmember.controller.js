/**
 * Created by Administrator on 2016-11-4.
 */
(function () {
  'use strict';
  angular.module('starter.controllers')
    .controller('EditMemberCtrl', ['$scope','LocalStorageService','$timeout',  function ($scope,LocalStorageService,$timeout) {


      $scope.members=LocalStorageService.getData('memberList', []);
      $scope.clearLocalStorage=function () {

        localStorage.removeItem('memberList')
      }
      var pageIndex=1;
      var isLoading=false;
      $scope.doRefresh=function () {
        pageIndex=1;
        if(isLoading){
          return;
        }
        isLoading=true;
        $timeout(function () {

          $scope.members=LocalStorageService.getData('memberList', []);
          isLoading=false;
          $scope.$broadcast('scroll.refreshComplete');

        },500);

      }


    }]);
})();
